using Microsoft.EntityFrameworkCore;
using Weather.DataBase;
using Weather.Repository;

var builder = WebApplication.CreateBuilder(args);
var services = builder.Services;

// ������.
builder.Host.ConfigureLogging((context, loggingBuilder) =>
{
    loggingBuilder.ClearProviders();
    loggingBuilder.AddConsole();
    loggingBuilder.AddFile(context.Configuration.GetSection("Logging"));

    loggingBuilder.SetMinimumLevel(LogLevel.Debug);

    if (OperatingSystem.IsWindows())
    {
        loggingBuilder.AddEventLog();
    }
});

services.AddControllersWithViews();

// ��������.
services.AddDbContext<Context>(options =>
                options.UseNpgsql("ConnectionString"));

// �����������.
services.AddRepository();

// SignalR.
services.AddSignalR();

var app = builder.Build();

if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    app.UseHsts();
}

app.UseHttpsRedirection();

app.UseStaticFiles();

app.UseRouting();


app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.MapHub<ProgressHub>("/progressHub");


app.Run();

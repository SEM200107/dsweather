﻿namespace Weather.Models
{
    public class WeatherViewModel
    {
        public DateTime Date { get; set; }
        public IEnumerable<Weather.DataBase.Entity.Weather> Weathers { get; set; }
        public int CurrentPage { get; set; }
        public int TotalPages { get; set; }
    }

}

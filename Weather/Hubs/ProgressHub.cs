﻿using Microsoft.AspNetCore.SignalR;

public class ProgressHub : Hub
{
    public async Task ReportProgress(int progress)
    {
        // Отправка прогресса всем подключенным клиентам
        await Clients.All.SendAsync("progress", progress);
    }

    public async Task ReportStatus(string status)
    {
        // Отправка статуса всем подключенным клиентам
        await Clients.All.SendAsync("status", status);
    }

    public async Task ReportNumberFile(string status)
    {
        // Отправка статуса всем подключенным клиентам
        await Clients.All.SendAsync("number", status);
    }
}

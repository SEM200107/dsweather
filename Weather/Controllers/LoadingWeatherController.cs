﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System.Globalization;
using Weather.Repository.Contracts;

namespace Weather.Controllers
{
    public class LoadingWeatherController : Controller
    {
        private readonly IWeatherRepository weatherRepository;
        private readonly IHubContext<ProgressHub> hubContext;
        private readonly ILogger<HomeController> logger;

        public LoadingWeatherController(IWeatherRepository weatherRepository, IHubContext<ProgressHub> hubContext, ILogger<HomeController> logger)
        {
            this.weatherRepository = weatherRepository;
            this.hubContext = hubContext;
            this.logger = logger;
        }
        public IActionResult LoadingWeather()
        {
            return View();
        }

        /// <summary>
        /// Загрузка погоды из файлов в БД.
        /// </summary>
        /// <param name="files">Файлы.</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> UploadFile(List<IFormFile> files)
        {
            try
            {
                for (var fileNumber = 0; fileNumber < files.Count; fileNumber++)
                {
                    // Отправляем на клиент номер загружаемого файла.
                    await hubContext.Clients.All.SendAsync("number", fileNumber+1);

                    // Открываем файл на чтение.
                    IWorkbook workbook;
                    var stream = files[fileNumber].OpenReadStream();
                    workbook = new XSSFWorkbook(stream);

                    for (int sheetNumber = 0; sheetNumber < 12; sheetNumber++)
                    {
                        // Отправляем на клиет номер загружаемого листа.
                        await hubContext.Clients.All.SendAsync("progress", sheetNumber);

                        // Считываем лист.
                        ISheet sheet = workbook.GetSheetAt(sheetNumber);

                        for (int row = 4; row <= sheet.LastRowNum; row++)
                        {
                            //получаем строку
                            var currentRow = sheet.GetRow(row);

                            if (currentRow != null)
                            {
                                var weatherModel = new Weather.DataBase.Entity.Weather();

                                // Формируем дату.
                                var dateString = currentRow.GetCell(0).StringCellValue;
                                var timeString = currentRow.GetCell(1).StringCellValue;
                                var format = "dd.MM.yyyy HH:mm";
                                var dateTimeString = string.Join(" ", dateString, timeString);

                                DateTime dateTime;
                                if (DateTime.TryParseExact(dateTimeString, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime))
                                {
                                    DateTime unspecified = DateTime.SpecifyKind(dateTime, DateTimeKind.Utc);
                                    weatherModel.DateTime = TimeZoneInfo.ConvertTimeToUtc(unspecified);
                                }
                                else
                                {
                                    throw new Exception("Ошибка при формировании даты");
                                }

                                // Парсим оставшуюся строку и запысываем значения в модель.
                                weatherModel.Temperature = currentRow.GetCell(2).CellType == CellType.String ? null : currentRow.GetCell(2).NumericCellValue;
                                weatherModel.Humidity = currentRow.GetCell(3).CellType == CellType.String ? null : currentRow.GetCell(3).NumericCellValue;
                                weatherModel.DewPoint = currentRow.GetCell(4).CellType == CellType.String ? null : currentRow.GetCell(4).NumericCellValue;
                                weatherModel.Pressure = currentRow.GetCell(5).CellType == CellType.String ? null : (int)currentRow.GetCell(5).NumericCellValue;
                                weatherModel.DirectionWind = currentRow.GetCell(6) == null ? "Нет информации" :
                                    currentRow.GetCell(6).StringCellValue == " " ? "Нет информации" : currentRow.GetCell(6).StringCellValue;
                                weatherModel.WindSpeed = currentRow.GetCell(7).CellType == CellType.String ? null : (int)currentRow.GetCell(7).NumericCellValue;
                                weatherModel.Cloudiness = currentRow.GetCell(8).CellType == CellType.String ? null : (int)currentRow.GetCell(8).NumericCellValue;
                                weatherModel.CloudHeight = currentRow.GetCell(9).CellType == CellType.String ? null : (int)currentRow.GetCell(9).NumericCellValue;
                                weatherModel.Horizon = currentRow.GetCell(10).CellType == CellType.String ? null : (int)currentRow.GetCell(10).NumericCellValue;
                                weatherModel.WeatherConditions = currentRow.GetCell(11) == null ? "Нет информации" :
                                    currentRow.GetCell(11).StringCellValue == " " ? "Нет информации" :
                                    currentRow.GetCell(11).StringCellValue == "" ? "Нет информации" : currentRow.GetCell(11).StringCellValue;

                                await weatherRepository.Add(weatherModel);
                            }
                        }
                    }
                }

                // Отправляем на клиет уведомление об успешной загрузки файлов.
                await hubContext.Clients.All.SendAsync("status", "Загрузка прошла успешно");
            }
            catch (Exception ex)
            {
                // Отправляем на клиет уведомление о не успешной загрузки файлов.
                await hubContext.Clients.All.SendAsync("status", "Не удалось загрузить файл");

                // Логгируем ошибку.
                logger.LogError(ex.ToString());
            }

            return View("LoadingWeather");
        }
    }
}

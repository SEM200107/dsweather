﻿using Microsoft.AspNetCore.Mvc;
using Weather.Models;
using Weather.Repository.Contracts;

namespace Weather.Controllers
{
    public class ViewWeatherController : Controller
    {
        private readonly IWeatherRepository weatherRepository;

        public ViewWeatherController(IWeatherRepository weatherRepository)
        {
            this.weatherRepository = weatherRepository;
        }

        /// <summary>
        /// Отображение погоды.
        /// </summary>
        /// <param name="year">Фильтр по году.</param>
        /// <param name="month">Фильтр по месяцу.</param>
        /// <param name="currentPage">Текущая страница.</param>
        /// <param name="pageSize">Кол-во элементов на странице.</param>
        /// <returns></returns>
        public async Task<IActionResult> ViewWeather(int year = 0, int month = 0, int currentPage = 1, int pageSize = 10)
        {
            // Полчаем погоду с учетом фильтров.
            var weathers = await weatherRepository.GetWeatherForFilters(year, month);

            // Собираем View модель для отображения.
            var groupedWeathers = weathers.GroupBy(w => w.DateTime.Date)
                .Select(g => new WeatherViewModel
                {
                    Date = g.Key,
                    Weathers = g.OrderBy(w => w.DateTime.TimeOfDay)
                    .ToList()
                })
                .OrderBy(w => w.Date).ToList();

            // Пагинация.
            var count = groupedWeathers.Count();
            var items = groupedWeathers.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
            var totalPages = (int)Math.Ceiling(count / (double)pageSize);

            ViewData["CurrentPage"] = currentPage;
            ViewData["PageSize"] = pageSize;
            ViewData["TotalPages"] = totalPages;
            ViewData["Year"] = year;
            ViewData["Month"] = month;

            return View(items);
        }
    }
}

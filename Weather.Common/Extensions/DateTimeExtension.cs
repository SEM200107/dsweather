﻿namespace Weather.Common.Extensions
{
    public static class DateTimeExtension
    {
        /// <summary>
        /// Метод преобразования UTC даты в часовой пояс Москвы
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns>Московское время</returns>
        public static DateTime ConvertToMoscowTz(this DateTime dateTime)
        {
            TimeZoneInfo moscowTimeZone = TimeZoneInfo.FindSystemTimeZoneById("Russian Standard Time");

            return TimeZoneInfo.ConvertTimeFromUtc(dateTime, moscowTimeZone);
        }
    }
}

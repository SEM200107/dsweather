﻿using Microsoft.Extensions.Configuration;

namespace Weather.DataBase
{
    public static class DbConfiguration
    {
        private static IConfigurationRoot configurationRoot;
        public static IConfigurationRoot Configuration
        {
            get
            {
                if (configurationRoot == null)
                {
                    configurationRoot = BuildConfiguration();
                }
                return configurationRoot;
            }
        }
        private static IConfigurationRoot BuildConfiguration()
        {
            var builder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
            return builder.Build();
        }
    }
}

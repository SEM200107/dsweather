﻿using System.ComponentModel.DataAnnotations;

namespace Weather.DataBase.Entity
{
    /// <summary>
    /// Базовая модель для всех сущностей.
    /// </summary>
    public class BaseEntity
    {
        /// <summary>
        /// Id сущности.
        /// </summary>
        [Key]
        public Guid Id { get; set; } = Guid.NewGuid();

        /// <summary>
        /// Дата создания сущьности.
        /// </summary>
        public DateTime DateOfCreation { get; set; } = DateTime.UtcNow;
    }
}

﻿namespace Weather.DataBase.Entity
{
    /// <summary>
    /// Модель погоды.
    /// </summary>
    public class Weather : BaseEntity
    {
        /// <summary>
        /// Дата и время.
        /// </summary>
        public DateTime DateTime { get; set; }

        /// <summary>
        /// Температура.
        /// </summary>
        public double? Temperature { get; set; }

        /// <summary>
        /// Относительная влажность воздуха.
        /// </summary>
        public double? Humidity { get; set; }

        /// <summary>
        /// Точка росы.
        /// </summary>
        public double? DewPoint { get; set; }

        /// <summary>
        /// Атмосферное давление.
        /// </summary>
        public int? Pressure { get; set; }

        /// <summary>
        /// Направление ветра.
        /// </summary>
        public string DirectionWind { get; set; }

        /// <summary>
        /// Скорость ветра.
        /// </summary>
        public int? WindSpeed { get; set; }

        /// <summary>
        /// Облачность.
        /// </summary>
        public int? Cloudiness { get; set; }

        /// <summary>
        /// Нижняя граница облачности.
        /// </summary>
        public int? CloudHeight { get; set; }

        /// <summary>
        /// Горизонтальная видимость.
        /// </summary>
        public int? Horizon {  get; set; }

        /// <summary>
        /// Погодные явления.
        /// </summary>
        public string WeatherConditions { get; set; }
    }
}

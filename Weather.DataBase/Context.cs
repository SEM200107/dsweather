﻿using Microsoft.EntityFrameworkCore;

namespace Weather.DataBase
{
    public class Context : DbContext
    {
        public DbSet<Weather.DataBase.Entity.Weather> Weathers { get; set; }
        public Context(DbContextOptions<Context> options)
            : base(options)
        {
            Database.EnsureCreated();
        }

        public Context(string connectionString)
            : base(GetOptions(connectionString))
        {
            Database.EnsureCreated();
        }

        private static DbContextOptions GetOptions(string connectionString)
        {
            var options = new DbContextOptionsBuilder();
            options.UseLazyLoadingProxies();
            return NpgsqlDbContextOptionsBuilderExtensions.UseNpgsql(options, connectionString).Options;
        }
    }
}
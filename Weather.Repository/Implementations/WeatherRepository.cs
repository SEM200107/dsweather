﻿using Microsoft.EntityFrameworkCore;
using Weather.Repository.Contracts;

namespace Weather.Repository.Implementations
{
    /// <summary>
    /// Репозиторий для работы с сущностью Weather.
    /// </summary>
    public class WeatherRepository : BaseRepository<Weather.DataBase.Entity.Weather>, IWeatherRepository
    {
        public async Task<List<DataBase.Entity.Weather>> GetWeatherForFilters(int year, int month)
        {
            var filter = DbContext.Weathers.AsQueryable();

            if (year != 0)
            {
                filter = filter.Where(w => w.DateTime.Year == year);
            }

            if (month != 0)
            {
                filter = filter.Where(w => w.DateTime.Month == month);
            }

            return await filter.ToListAsync();
        }

    }
}

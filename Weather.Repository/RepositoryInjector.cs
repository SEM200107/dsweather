﻿using Microsoft.Extensions.DependencyInjection;
using Weather.Repository.Contracts;
using Weather.Repository.Implementations;

namespace Weather.Repository
{
    public static class RepositoryInjector
    {
        public static void AddRepository(this IServiceCollection services)
        {
            services.AddScoped<IWeatherRepository, WeatherRepository>();
        }
    }
}
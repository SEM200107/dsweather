﻿namespace Weather.Repository.Contracts
{
    /// <summary>
    /// Базовый интерфейс всех репозиториев.
    /// </summary>
    /// <typeparam name="T">Сущьность БД.</typeparam>
    public interface IBaseRepository<T> where T : class
    {
        /// <summary>
        /// Получить сущность по id.
        /// </summary>
        /// <param name="id">Id сущности в БД.</param>
        /// <returns>Сущность.</returns>
        Task<T> GetById(Guid id);

        /// <summary>
        /// Получить сущность по набору id.
        /// </summary>
        /// <param name="ids">Коллекция id'шников сущностей в БД.</param>
        /// <returns>Коллекция сущностей.</returns>
        Task<List<T>> GetByIdList(List<Guid> ids);

        /// <summary>
        /// Получить все сущности из БД.
        /// </summary>
        /// <returns>Коллекция сущностей.</returns>
        Task<List<T>> GetAll();

        /// <summary>
        /// Добавить сущность в БД.
        /// </summary>
        /// <param name="entity">Сущность.</param>
        Task Add(T entity);

        /// <summary>
        /// Добавить несколько сущностей в БД.
        /// </summary>
        /// <param name="entities">Коллекция сущностей.</param>
        Task AddRange(IEnumerable<T> entities);

        /// <summary>
        /// Удалить сущность из БД.
        /// </summary>
        /// <param name="entity">Сущность.</param>
        Task Remove(T entity);

        /// <summary>
        /// Удалить несколько сущностей в БД.
        /// </summary>
        /// <param name="entities">Коллекция сущностей.</param>
        Task RemoveRange(IEnumerable<T> entities);

        /// <summary>
        /// Обновить сущность в БД.
        /// </summary>
        /// <param name="entity">Сущность.</param>
        Task Update(T entity);
    }
}

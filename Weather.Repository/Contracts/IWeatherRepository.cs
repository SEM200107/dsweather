﻿namespace Weather.Repository.Contracts
{
    /// <summary>
    /// Интерфейс репозитория для раьоты с сущностью Weather.
    /// </summary>
    public interface IWeatherRepository : IBaseRepository<Weather.DataBase.Entity.Weather>
    {
        /// <summary>
        /// Получение погоды с учетом фильтров.
        /// </summary>
        /// <param name="year">Фильтр по году.</param>
        /// <param name="month">Фильтр по месяцу.</param>
        /// <returns>Коллекция погоды.</returns>
        Task<List<Weather.DataBase.Entity.Weather>> GetWeatherForFilters(int year, int month);
    }
}
